using Swapfiets.Reports.Thefts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Swapfiets.Reports.Thefts.Common;

namespace Swapfiets.Reports.BlazorServer.Data
{
    public class TheftFetchingService
    {
        private ITheftDataFetcher _theftDataFetcher;
        public TheftFetchingService(ITheftDataFetcher theftDataFetcher)
        {
            _theftDataFetcher = theftDataFetcher;
        }
        
        private static readonly Dictionary<string, int> constantCities = new Dictionary<string, int>
        {
            { "Amsterdam", 0 },
            { "Berlin", 0 },
            { "Copenhagen", 0 },
            { "Brussels", 0 },
            { "Milan", 0 },
            { "London", 0 },
            { "Paris", 0 },
        };

        public async Task<TheftResponse[]> GetConstantCitiesAsync()
        {
            var tasks = constantCities.Keys.Select(city => _theftDataFetcher.GetNumberOfTheftsAsync(city)).ToList();
            return await Task.WhenAll(tasks);
        }

        public async Task<TheftResponse> GetTheftsForLocationParams(string locationParameters)
        {
            return await _theftDataFetcher.GetNumberOfTheftsAsync(locationParameters);
        }
    }
}
