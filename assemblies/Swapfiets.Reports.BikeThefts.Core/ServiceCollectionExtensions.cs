﻿using System;
using Microsoft.Extensions.DependencyInjection;
using Swapfiets.Reports.Thefts.BikeThefts;
using Swapfiets.Reports.Thefts.Common;

namespace Swapfiets.Reports.Thefts
{
    public static class ServiceCollectionExtensions
    {
        public static void AddBikeIndexDependencies(this IServiceCollection services)
        {
            services.AddHttpClient("BikeIndexClient", httpClient =>
            {
                httpClient.BaseAddress = new UriBuilder("https://bikeindex.org:443/api/v3/"){ Port = 443}.Uri;
            });
            services.AddSingleton<ITheftDataFetcher, BikeIndexTheftDataFetcher>();
        }
    }
}
