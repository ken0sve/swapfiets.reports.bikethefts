﻿using System;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Swapfiets.Reports.Thefts.Common;

namespace Swapfiets.Reports.Thefts.BikeThefts
{
    /// <summary>
    /// Queries BikeIndex search API to retrieve stolen bike reports.
    /// </summary>
    public class BikeIndexTheftDataFetcher : ITheftDataFetcher
    {
        private readonly ILogger<BikeIndexTheftDataFetcher> _logger;
        private readonly IHttpClientFactory _httpClientFactory;

        public BikeIndexTheftDataFetcher(IHttpClientFactory httpClientFactory, ILogger<BikeIndexTheftDataFetcher> logger)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _httpClientFactory = httpClientFactory ?? throw new ArgumentNullException(nameof(httpClientFactory));
        }

        public async Task<TheftResponse> GetNumberOfTheftsAsync(string locationParameters)
        {
            if (string.IsNullOrWhiteSpace(locationParameters))
            {
                _logger.LogError("Location parameter is null or empty, won't make request to Bike Index.");
                return new TheftResponse(null, "At least one location parameter must be provided.");
            }

            try
            {
                var httpClient = _httpClientFactory.CreateClient("BikeIndexClient");
                using var httpResponseMessage = await httpClient.GetAsync($"search/count?location={locationParameters}");

                httpResponseMessage.EnsureSuccessStatusCode();

                var responseContent = await httpResponseMessage.Content.ReadAsStringAsync();
                var count = JsonDocument.Parse(responseContent).RootElement.GetProperty("proximity").GetInt32();
                _logger.LogInformation($"BikeIndex API returned {count} theft reports for location parameter {locationParameters}.");
                return new TheftResponse(locationParameters, count);

            }
            catch (HttpRequestException e)
            {
                _logger.LogError($"BikeIndex API returned {e.Message} when fetching reports for {locationParameters}.");
                return new TheftResponse(locationParameters, $"Failed to fetch bike theft reports for location parameters '{locationParameters}' from Bike Index. The remote endpoint returned: \"{e.Message}\"");
            }
            catch (Exception e)
            {
                _logger.LogError(e, $"Caught exception while fetching reports for location parameters '{locationParameters}' from Bike Index");
                return new TheftResponse(locationParameters, $"Caught exception while fetching reports for location parameters '{locationParameters}' from Bike Index, see log file for details: {e.Message}");
            }
        }
    }
}
