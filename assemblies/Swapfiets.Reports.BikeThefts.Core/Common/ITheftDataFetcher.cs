﻿using System.Threading.Tasks;

namespace Swapfiets.Reports.Thefts.Common
{
    public interface ITheftDataFetcher
    {
        /// <summary>
        /// Get the number of reported thefts.
        /// </summary>
        /// <param name="locationParameters">Set of location parameters (e.g. city, county, state, region, country, etc.)</param>
        /// <returns>A <see cref="TheftResponse"/> that contains the number of thefts or the error message in case of failure.</returns>
        Task<TheftResponse> GetNumberOfTheftsAsync(string locationParameters);
    }
}
