﻿namespace Swapfiets.Reports.Thefts.Common
{
    public class TheftResponse
    {
        public TheftResponse(string location, string errorMessage)
        {
            Location = location;
            ErrorMessage = errorMessage;
            NumberOfReports = -1;
        }

        public TheftResponse(string location, int numberOfReports)
        {
            Location = location;
            NumberOfReports = numberOfReports;
        }

        public int NumberOfReports { get; }
        public string ErrorMessage { get; }
        public string Location { get; }
    }
}