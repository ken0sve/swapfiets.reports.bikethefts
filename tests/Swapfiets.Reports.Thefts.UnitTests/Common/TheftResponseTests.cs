﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using Swapfiets.Reports.Thefts.Common;

namespace Swapfiets.Reports.Thefts.UnitTests.Common
{
    [TestFixture]
    public class TheftResponseTests
    {
        [Test]
        public void ConstructorTest()
        {
            var subject = new TheftResponse("test location", 10);
            Assert.AreEqual("test location", subject.Location);
            Assert.AreEqual(10, subject.NumberOfReports);
            Assert.AreEqual(null, subject.ErrorMessage);

            subject = new TheftResponse("test location", "test error");
            Assert.AreEqual("test location", subject.Location);
            Assert.AreEqual("test error", subject.ErrorMessage);
            Assert.AreEqual(-1, subject.NumberOfReports);
        }
    }
}
