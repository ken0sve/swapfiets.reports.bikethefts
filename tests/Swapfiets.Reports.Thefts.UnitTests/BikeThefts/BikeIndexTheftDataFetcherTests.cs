﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging.Abstractions;
using Moq;
using Moq.Protected;
using NUnit.Framework;
using Swapfiets.Reports.Thefts.BikeThefts;

namespace Swapfiets.Reports.Thefts.UnitTests.BikeThefts
{
    [TestFixture]
    public class BikeIndexTheftDataFetcherTests
    {
        private MockRepository _mockRepository;
        private Mock<HttpMessageHandler> _httpMessageHandlerMock;
        private Mock<IHttpClientFactory> _httpClientFactoryMock;

        [SetUp]
        public void SetUp()
        {
            _mockRepository = new MockRepository(MockBehavior.Default) { DefaultValue = DefaultValue.Mock };
            _httpMessageHandlerMock = _mockRepository.Create<HttpMessageHandler>();
            _httpClientFactoryMock = _mockRepository.Create<IHttpClientFactory>();
        }

        [TearDown]
        public void TearDown()
        {
            _mockRepository.VerifyAll();
        }

        [Test]
        public void HappyFlowPath_Test()
        {
            var httpClient = new HttpClient(_httpMessageHandlerMock.Object) { BaseAddress = new Uri("https://localhost") };
            _httpClientFactoryMock.Setup(mock => mock.CreateClient("BikeIndexClient")).Returns(httpClient);

            const string locationParams = "test location";

            // Return HTTP 200 with 10 stolen reports for location.
            var httpResponseMessage = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent("{\"non\": 124124,\"stolen\": 123,\"proximity\": 10}")
            };
            _httpMessageHandlerMock.Protected().Setup<Task<HttpResponseMessage>>(
                "SendAsync",
                ItExpr.IsAny<HttpRequestMessage>(),
                ItExpr.IsAny<CancellationToken>()
            ).Returns(Task.FromResult(httpResponseMessage));


            var subject = new BikeIndexTheftDataFetcher(_httpClientFactoryMock.Object, NullLogger<BikeIndexTheftDataFetcher>.Instance);

            var result = subject.GetNumberOfTheftsAsync(locationParams).Result;

            Assert.IsNotNull(result, "Expected non-null result.");
            Assert.AreEqual(10, result.NumberOfReports);
            Assert.AreEqual(locationParams, result.Location);
            Assert.IsNull(result.ErrorMessage, "Expected null error message for successful request.");
        }

        [Test]
        public void NotSuccessfulResponseFromBikeIndex_Test()
        {
            var httpClient = new HttpClient(_httpMessageHandlerMock.Object) { BaseAddress = new Uri("https://localhost") };
            _httpClientFactoryMock.Setup(mock => mock.CreateClient("BikeIndexClient")).Returns(httpClient);

            const string locationParams = "test location";

            // Return HTTP 200 with 10 stolen reports for location.
            var httpResponseMessage = new HttpResponseMessage(HttpStatusCode.InternalServerError)
            {
                ReasonPhrase = "Test failure reason."
            };
            _httpMessageHandlerMock.Protected().Setup<Task<HttpResponseMessage>>(
                "SendAsync",
                ItExpr.IsAny<HttpRequestMessage>(),
                ItExpr.IsAny<CancellationToken>()
            ).Returns(Task.FromResult(httpResponseMessage));


            var subject = new BikeIndexTheftDataFetcher(_httpClientFactoryMock.Object, NullLogger<BikeIndexTheftDataFetcher>.Instance);

            var result = subject.GetNumberOfTheftsAsync(locationParams).Result;

            Assert.IsNotNull(result, "Expected non-null result.");
            Assert.AreEqual(-1, result.NumberOfReports, "Expected -1 number of thefts in result for not successful request.");
            Assert.AreEqual(locationParams, result.Location);
            Assert.IsTrue(result.ErrorMessage.Contains(httpResponseMessage.ReasonPhrase), "Expected response reason to be propagated.");
        }

        [Test]
        public void ExceptionWhileMakingRequest_Test()
        {
            var httpClient = new HttpClient(_httpMessageHandlerMock.Object) { BaseAddress = new Uri("https://localhost") };
            _httpClientFactoryMock.Setup(mock => mock.CreateClient("BikeIndexClient")).Returns(httpClient);

            const string locationParams = "test location";

            var exception = new Exception("Unexpected exception.");
            _httpMessageHandlerMock.Protected().Setup<Task<HttpResponseMessage>>(
                "SendAsync",
                ItExpr.IsAny<HttpRequestMessage>(),
                ItExpr.IsAny<CancellationToken>()
            ).Throws(exception);


            var subject = new BikeIndexTheftDataFetcher(_httpClientFactoryMock.Object, NullLogger<BikeIndexTheftDataFetcher>.Instance);

            var result = subject.GetNumberOfTheftsAsync(locationParams).Result;

            Assert.IsNotNull(result, "Expected non-null result.");
            Assert.AreEqual(-1, result.NumberOfReports, "Expected -1 number of thefts in result for not successful request.");
            Assert.AreEqual(locationParams, result.Location);
            Assert.IsTrue(result.ErrorMessage.Contains(exception.Message), "Expected response reason to be propagated.");
        }
    }
}
