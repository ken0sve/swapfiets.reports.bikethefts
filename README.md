## Introduction

This project contains a very basic class library to query registered thefts (currently only bike thefts from Bike Index API) and a very basic Blazor app that uses it.

# Reference links

You need dotnet core version 3.1 to build this solution and run the Blazor project.
You can navigate to the solution/project folder you want to build/run and use the dotnet CLI.